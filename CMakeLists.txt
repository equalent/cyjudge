cmake_minimum_required(VERSION 3.11.0)

project(CyJudge
	DESCRIPTION "CyJudge"
	VERSION 1.1.1
)

file(GLOB SRC_FILES Source/*.cpp Source/*.h Source/*.hpp Source/*.c)
file(GLOB INC_FILES Include/*.h Include/*.hpp)

add_executable(CyJudge-Daemon ${SRC_FILES} ${INC_FILES})